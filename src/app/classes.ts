export class Player {
  id:number;
  firstName:string;
  lastName:string;
  position:string;
  phone:string;
  teamID:number;
  number:number;
}

export class Team {
  id:number;
  name:string;
  hometown:string;
  captain:Player;
}

export class Stat {
  id:number;
  playerID:number;
  label:string;
  value:number;
}

export class Match {
  id:number;
  team1Id:number;
  team2Id:number;
  time:number;
  geocode:string;
  scoreTeam1:number;
  scoreTeam2:number;
  isOver:boolean;
}
